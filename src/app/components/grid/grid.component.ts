import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
  breakpoint: number | undefined;
  squareArray: any;
  constructor() {
  this.squareArray=  Array.from(Array(8), () => new Array(8));
 }

  ngOnInit() {
    
  }

  //---methods go down here---
 

}
